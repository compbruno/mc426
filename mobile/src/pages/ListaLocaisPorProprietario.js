import React, { Component } from 'react';
import api from '../services/api';

import {
    View,
    Text,
    StyleSheet,
    ScrollView,
    Button,
} from 'react-native';

import { FlatList, TouchableOpacity } from 'react-native-gesture-handler';

import Header from '../components/Header'

import FBSDK from 'react-native-fbsdk'
const { AccessToken } = FBSDK

export default class ListaLocaisPorProprietario extends Component {
    state = {
        locais: [],
        proprietarioFBID: "",
        proprietarioID: ""
    };

    async componentDidMount() {
        AccessToken.getCurrentAccessToken()
            .then((response) => {
                fetch('https://graph.facebook.com/v2.5/me?access_token=' + response.accessToken)
                    .then((response2) => response2.json())
                    .then((json) => {
                        this.setState({
                            proprietarioFBID: json.id
                        })
                    })
                    .then(async () => {
                        try {
                            const response = await api.get("proprietario/facebookID/" + this.state.proprietarioFBID);
                            this.setState({
                                proprietarioID: response.data._id
                            });
                        } catch (error) {
                            console.log(error)
                        }
                    })
                    .then(async () => {
                        try {
                            const response = await api.get('/proprietario/facebookID/' + this.state.proprietarioFBID);
                            this.setState({ locais: response.data.locais });
                        } catch (error) {
                            console.log(error)
                        }
                    })
                    .done();
            })
            .catch(error => {
                console.log(error)
            })

    }

    handleAdicionarLocal = () => {
        this.props.navigation.navigate("CadastroLocal");
    }

    handleVoltar = () => {
        this.props.navigation.navigate("ProprietarioInfo")
    }

    renderItem = ({ item }) => (
        <View style={styles.localContainer}>
            <TouchableOpacity
                style={styles.localButton}
            >
                <Text style={styles.localBairro}>{item.endereco.bairro}</Text>
                <Text style={styles.localInfo}>{item.endereco.logradouro}</Text>
                <Text style={styles.localInfo}>{item.endereco.numero}</Text>
                <Text style={styles.localInfo}>{item.endereco.cep}</Text>
            </TouchableOpacity>
        </View>
    )

    render() {
        return (
            <ScrollView behavior="padding" style={styles.container}>
                <Header />
                <View>
                    <Text style={styles.titulo}>Seus locais:</Text>
                </View>
                <View style={styles.btnAdicionarLocal}>
                    <Button
                        onPress={() => {
                            this.handleAdicionarLocal();
                        }}
                        title="Adicionar Local +"
                        fontSize="18"
                        color="#36B67E"
                        fontWeight="bold"
                    />
                </View>
                <View>
                    <FlatList
                        contentContainerStyle={styles.list}
                        data={this.state.locais}
                        keyExtractor={item => item._id}
                        renderItem={this.renderItem}
                    />
                </View>
                <View style={styles.btnAdicionarLocal}>
                    <Button
                        onPress={() => {
                            this.handleVoltar();
                        }}
                        title="Voltar"
                        fontSize="18"
                        color="#36B67E"
                        fontWeight="bold"
                    />
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFFFFF',
    },
    buttonCadastroProp: {
        backgroundColor: '#FFF',
        width: 205,
        height: 50,
        borderRadius: 2,
        textAlign: 'center',
        paddingTop: 15,
        marginTop: 15,
        left: 35,
        color: '#36B67E',
        borderWidth: 0.75,
        borderColor: '#36B77E',
        marginBottom: 15
    },
    list: {
        padding: 20
    },

    localContainer: {
        backgroundColor: "#FAFAFA",
        borderWidth: 1,
        borderColor: "#DDD",
        borderRadius: 5,
        padding: 20,
        marginBottom: 20
    },

    localBairro: {
        fontSize: 18,
        fontWeight: "bold",
        color: "#333"
    },

    localInfo: {
        fontSize: 16,
        color: "#999",
        marginTop: 5,
        lineHeight: 24
    },

    btnAdicionarLocal: {
        height: 50,
        width: 175,
        borderColor: "#36B67E",
        borderWidth: 0.75,
        borderRadius: 5,
        justifyContent: "center",
        alignItems: "center",
        margin: 10
    },

    btnAdicionarLocalText: {
        fontSize: 18,
        color: "#36B67E",
        fontWeight: "bold"
    },

    titulo: {
        fontSize: 20,
        color: "#333",
        margin: 15
    }
});