import React, { Component } from "react";
import api from '../services/api';

import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  ScrollView
} from 'react-native';

import FBSDK from 'react-native-fbsdk'
const { AccessToken } = FBSDK

import { Input } from 'react-native-elements';

import Header from '../components/Header'

export default class CadastroLocal extends Component {
  state = {
    endereco: {
      logradouro: "",
      numero: "",
      bairro: "",
      cidade: "",
      cep: "",
      uf: "",
      pais: "",
      latitude: "",
      longitude: "",
    },
    precoDia: "",
    proprietarioFBID: "",
    proprietarioID: "",
    proprietario: null
  };

  async componentDidMount() {
    AccessToken.getCurrentAccessToken()
      .then((response) => {
        fetch('https://graph.facebook.com/v2.5/me?access_token=' + response.accessToken)
          .then((response2) => response2.json())
          .then((json) => {
            this.setState({
              proprietarioFBID: json.id
            })
          })
          .then(async () => {
            try {
              const response = await api.get("proprietario/facebookID/" + this.state.proprietarioFBID);
              this.setState({
                proprietarioID: response.data._id,
                proprietario: response.data
              });
            } catch (error) {
              console.log(error)
            }
          })
          
          .done();
      })
      .catch(error => {
        console.log(error)
      })
  }

  handleCompletarCadastro = async local => {
    const logradouro = this.state.endereco.logradouro;
    const numero = this.state.endereco.numero;
    const bairro = this.state.endereco.bairro;
    const cidade = this.state.endereco.cidade;
    const cep = this.state.endereco.cep;
    const uf = this.state.endereco.uf;
    const pais = this.state.endereco.pais;
    const latitude = this.state.endereco.latitude;
    const longitude = this.state.endereco.longitude;
    const precoDia = this.state.precoDia;
    const proprietario = this.state.proprietario;
    const local0 = await api.post(`proprietario/${this.state.proprietarioID}/local`, { endereco:{logradouro, numero, bairro, cidade, cep, uf, pais, latitude, longitude}, precoDia, proprietario });
    this.props.navigation.navigate("ProprietarioInfo")
  }

  handleLogradouroChange = logradouro => {
    this.setState({
      ...this.state,
      endereco: {
        ...this.state.endereco,
        logradouro: logradouro
      }
    });
  }

  handleNumeroChange = numero => {
    this.setState({
      ...this.state,
      endereco: {
        ...this.state.endereco,
        numero: numero
      }
    });
  }

  handleCidadeChange = cidade => {
    this.setState({
      ...this.state,
      endereco: {
        ...this.state.endereco,
        cidade: cidade
      }
    });
  }

  handleBairroChange = bairro => {
    this.setState({
      ...this.state,
      endereco: {
        ...this.state.endereco,
        bairro: bairro
      }
    });
  }

  handleCepChange = cep => {
    this.setState({
      ...this.state,
      endereco: {
        ...this.state.endereco,
        cep: cep
      }
    });
  }

  handleUfChange = uf => {
    this.setState({
      ...this.state,
      endereco: {
        ...this.state.endereco,
        uf: uf
      }
    });
  }

  handlePaisChange = pais => {
    this.setState({
      ...this.state,
      endereco: {
        ...this.state.endereco,
        pais: pais
      }
    });
  }

  handleLatitudeChange = latitude => {
    this.setState({
      ...this.state,
      endereco: {
        ...this.state.endereco,
        latitude: latitude
      }
    });
  }

  handleLongitudeChange = longitude => {
    this.setState({
      ...this.state,
      endereco: {
        ...this.state.endereco,
        longitude: longitude
      }
    });
  }

  handlePrecoDiaChange = precoDia => {
    this.setState({
      ...this.state,
      precoDia
    });
  }

  handleCadastrarComoCliente = () => {
    this.props.navigation.navigate("CadastroCliente")
  }

  handleCancelar = () => {
    this.props.navigation.navigate("ProprietarioInfo")
  }

  render() {
    return (
      <ScrollView>
        <Header />
        <View style={styles.bodyBox}>
          <Text style={styles.textWelcome}>Envie as suas informações, ache um local</Text>
          <Text style={styles.textWelcome2}>e pode deixar com a gente que fazemos o resto! :)</Text>

          <View style={styles.formCadastro}>
            <Text style={styles.textTipoLocal}>Cadastro do local</Text>
            <Input style={styles.inputNome} placeholder="Logradouro" onChangeText={this.handleLogradouroChange} value={this.state.endereco.logradouro} />
            <Input style={styles.inputNome} placeholder="Numero" onChangeText={this.handleNumeroChange} value={this.state.endereco.numero} />
            <Input style={styles.inputNome} placeholder="Cidade" onChangeText={this.handleCidadeChange} value={this.state.endereco.cidade} />
            <Input style={styles.inputNome} placeholder="Bairro" onChangeText={this.handleBairroChange} value={this.state.endereco.bairro} />
            <Input style={styles.inputNome} placeholder="Cep" onChangeText={this.handleCepChange} value={this.state.endereco.cep} />
            <Input style={styles.inputNome} placeholder="Uf" onChangeText={this.handleUfChange} value={this.state.endereco.uf} />
            <Input style={styles.inputNome} placeholder="Pais" onChangeText={this.handlePaisChange} value={this.state.endereco.pais} />
            <Input style={styles.inputNome} placeholder="Latitude" onChangeText={this.handleLatitudeChange} value={this.state.endereco.latitude} />
            <Input style={styles.inputNome} placeholder="Longitude" onChangeText={this.handleLongitudeChange} value={this.state.endereco.longitude} />
            <Input style={styles.inputNome} placeholder="Preço/Dia" onChangeText={this.handlePrecoDiaChange} value={this.state.precoDia} />
            <TouchableOpacity onPress={this.handleCompletarCadastro}><Text style={styles.buttonCompletarCadastro}>Completar Cadastro</Text></TouchableOpacity>
            <TouchableOpacity onPress={this.handleCancelar}><Text style={styles.buttonCadastroProp}>Cancelar</Text></TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  bodyBox: {
    marginTop: 20
  },
  buttonGroup: {
    flexDirection: 'row',
    margin: 15
  },
  buttonCadastroProp: {
    backgroundColor: '#FFF',
    width: 205,
    height: 50,
    borderRadius: 2,
    textAlign: 'center',
    paddingTop: 15,
    marginTop: 15,
    left: 35,
    color: '#36B67E',
    borderWidth: 0.75,
    borderColor: '#36B77E',
    marginBottom: 15
  },
  buttonTipoImovel: {
    marginLeft: 10,
    marginTop: 10
  },
  buttonCompletarCadastro: {
    backgroundColor: '#36B67E',
    width: 205,
    height: 50,
    borderRadius: 2,
    textAlign: 'center',
    paddingTop: 15,
    marginTop: 25,
    left: 35,
    color: '#FFF'
  },
  buttonRealizarEvento: {
    backgroundColor: '#FFF',
    width: 205,
    height: 50,
    borderRadius: 2,
    textAlign: 'center',
    paddingTop: 15,
    marginTop: 15,
    left: 35,
    color: '#36B67E',
    borderWidth: 0.75,
    borderColor: '#36B77E',
    marginBottom: 15
  },
  bannerAnuncio: {
    backgroundColor: '#5063f0',
    height: 100
  },
  formCadastro: {
    margin: 36,
    width: 300,
    borderColor: '#E5E5E5',
    borderWidth: 0.75,
    borderRadius: 2,
  },
  buttonText: {
    color: '#FFF',
    width: 205,
    height: 50,
    top: 30,
    left: 77,
    borderColor: '#FFF',
    borderWidth: 0.75,
    borderRadius: 2,
    textAlign: 'center',
    paddingTop: 15
  },
  textWelcome: {
    textAlign: 'center',
    fontSize: 18,
    fontWeight: 'bold',
    marginTop: 5
  },
  textWelcome2: {
    textAlign: 'center',
    fontSize: 14,
    marginTop: 5
  },
  textTipoLocal: {
    textAlign: 'center',
    fontSize: 14,
    fontWeight: 'bold',
    marginTop: 10
  },
  textInput: {
    width: 260,
    height: 50,
    margin: 60,
    borderWidth: 1,
    borderRadius: 2,
    borderColor: '#ddd',
    borderBottomWidth: 0,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1,
    padding: 10,
  },
  landscape: {
    marginLeft: 5
  }
});