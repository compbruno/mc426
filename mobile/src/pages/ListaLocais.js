import React, { Component } from 'react';
import api from '../services/api';

import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  BackHandler
} from 'react-native';

import { Button } from 'react-native-elements'


import { FlatList } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/FontAwesome';

import Header from '../components/Header'

export default class ListaLocais extends Component {
  constructor(props) {
    super(props);
  }

  state = {
    locais: []
  };


  deleteLocal = (local) => {
    const idLocal = local['_id'];
    const idProprietario = local['proprietario'];

    api.delete('proprietario/' + idProprietario + '/local/' + idLocal)
      .then((res) => {
        this.removeLocalState(this.state.locais, idLocal);

      })
      .catch((error) => {
        console.log(error);
      });
  };

  removeLocalState = (stateLocal, idLocal) => {
    stateLocal.forEach((local, index) => {
      if (local._id == idLocal) {
        stateLocal.splice(index, 1);
      }
    })

    this.setState({ locais: stateLocal });
  }

  async componentDidMount() {
    const response = await api.get("locais");
    this.setState({ locais: response.data });
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
  }

  handleBackPress = () => {
    this.props.navigation.navigate("Home");
    return true;
  }

  handleAdicionarLocal = () => {
    this.props.navigation.navigate("CadastroLocal");
  }

  handleVoltar = () => {
    this.props.navigation.navigate("Home");
  }

  detailLocal = (local) => {
    console.log("id = " + local._id);
    this.props.navigation.navigate("DetalheLocal", {idLocal: local._id});
  }

  renderItem = ({ item }) => (
    <View style={styles.localContainer}
    
    >

      <View style={styles.viewDados}
      >
        <TouchableOpacity
          style={styles.localButton}
          //onPress={() => this.detailLocal(item)}
        >
          <Text style={styles.localBairro}>{item.titulo}</Text>
          <Text style={styles.localInfo}>{item.endereco.bairro}</Text>
          <Text style={styles.localInfo}>{item.endereco.logradouro}</Text>
          <Text style={styles.localInfo}>{item.endereco.numero}</Text>
          <Text style={styles.localInfo}>{item.endereco.cep}</Text>

        </TouchableOpacity>
      </View>

      <View style={[{ width: "20%", backgroundColor: "", height: "50%" }]}>
        <Button
          onPress={() => this.deleteLocal(item)}
          title=""
          icon={
            <Icon
              name="window-close"
              size={15}
              color="white"
            />
          }
        />
      </View>

    </View>

  )

  render() {
    return (
      <ScrollView behavior="padding" style={styles.container}>
        <Header />
        <View>
          <Text style={styles.titulo}>Locais cadastrados:</Text>
        </View>
        <View>
          <FlatList
            contentContainerStyle={styles.list}
            data={this.state.locais}
            keyExtractor={item => item._id}
            renderItem={this.renderItem}
          />
        </View>
        <View>
          <TouchableOpacity onPress={this.handleVoltar}><Text style={styles.buttonCadastroProp}>Voltar</Text></TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },

  list: {
    padding: 20
  },

  localContainer: {
    backgroundColor: "#FAFAFA",
    borderWidth: 1,
    borderColor: "#DDD",
    borderRadius: 5,
    padding: 20,
    marginBottom: 20,
    flexDirection: 'row'
  },

  localBairro: {
    fontSize: 18,
    fontWeight: "bold",
    color: "#333"
  },

  localInfo: {
    fontSize: 16,
    color: "#999",
    marginTop: 5,
    lineHeight: 24
  },

  btnAcicionarLocal: {
    height: 50,
    width: 175,
    borderColor: "#36B67E",
    borderWidth: 0.75,
    borderRadius: 5,
    justifyContent: "center",
    alignItems: "center",
    margin: 10
  },
  buttonCadastroProp: {
    backgroundColor: '#FFF',
    width: 205,
    height: 50,
    borderRadius: 2,
    textAlign: 'center',
    paddingTop: 15,
    marginTop: 15,
    left: 35,
    color: '#36B67E',
    borderWidth: 0.75,
    borderColor: '#36B77E',
    marginBottom: 15
  },
  btnAcicionarLocalText: {
    fontSize: 18,
    color: "#36B67E",
    fontWeight: "bold"
  },

  titulo: {
    fontSize: 20,
    color: "#333",
    margin: 15
  },

  btnDelete: {
    color: '#333',
    flexDirection: 'row'
  },

  viewDados: {
    width: "80%"
  },
  viewBtn: {
    width: "20%",
    flexDirection: 'row'
  }
});