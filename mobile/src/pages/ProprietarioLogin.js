import React, { Component } from 'react';
import { facebookService } from '../services/FacebookService'
import {
  ScrollView,
  View,
  Text,
  StyleSheet,
  BackHandler
} from 'react-native';
import Header from '../components/Header'

import FBSDK from 'react-native-fbsdk'
const { AccessToken } = FBSDK

export default class ProprietarioLogin extends Component {
  constructor(props) {
    super(props)

    this.login = this.login.bind(this)
  }

  login() {
    this.props.navigation.navigate('ProprietarioSwitch');
  }

  async loadData() {
    const profile = await facebookService.fetchProfile();

    this.setState({
      profile: profile
    });
  }

  handleBackPress = () => {
    this.props.navigation.navigate("Home");
    return true;
  }

  render() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    return (
      <ScrollView behavior="padding" style={styles.container}>
        <Header />
        <View style={styles.bodyBox}>
          <Text style={styles.textWelcome2}>Acesso como proprietário</Text>
          <Text style={styles.textWelcome}>Antes de continuar, identifique-se</Text>
        </View>
        <View style={styles.container}>
          {facebookService.makeLoginButton((accessToken) => {
            this.login()
          })}
        </View>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  bodyBox: {
    marginTop: 20
  },
  bannerAnuncio: {
    backgroundColor: '#5063f0',
    height: 100
  },
  buttonText: {
    color: '#FFF',
    width: 205,
    height: 50,
    top: 30,
    left: 77,
    borderColor: '#FFF',
    borderWidth: 0.75,
    borderRadius: 2,
    textAlign: 'center',
    paddingTop: 15
  },
  textWelcome: {
    textAlign: 'center',
    fontSize: 18,
    fontWeight: 'bold',
    marginTop: 5
  },
  textWelcome2: {
    textAlign: 'center',
    fontSize: 14,
    marginTop: 5
  },
  textInput: {
    width: 260,
    height: 50,
    margin: 60,
    borderWidth: 1,
    borderRadius: 2,
    borderColor: '#ddd',
    borderBottomWidth: 0,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1,
    padding: 10,
  },
  landscape: {
    marginLeft: 5,
    marginTop: 170
  }
})