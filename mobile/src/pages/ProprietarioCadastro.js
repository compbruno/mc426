import React, { Component } from "react";
import api from '../services/api';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  ScrollView
} from 'react-native';

import { Input } from 'react-native-elements';
import Header from '../components/Header'

import { facebookService } from '../services/FacebookService'
import FBSDK from 'react-native-fbsdk';
const { AccessToken } = FBSDK

export default class CadastroProprietario extends Component {
  state = {
    nome: "",
    email: "",
    telefone: "",
    cpf: "",
    senha: "",
    facebookID: "",
  };

  async componentDidMount() {
    AccessToken.getCurrentAccessToken()
      .then((response) => {
        fetch('https://graph.facebook.com/v2.5/me?access_token=' + response.accessToken)
          .then((response2) => response2.json())
          .then((json) => {
            this.setState({
              facebookID: json.id,
              nome: json.name
            })
          })
          .done();
      })
      .catch(error => {
        console.log(error)
      })
  }

  handleCadastroProprietario = async usuario => {
    const nome = this.state.nome;
    const email = this.state.email;
    const telefone = this.state.telefone;
    const cpf = this.state.cpf;
    const facebookID = this.state.facebookID

    console.log("Proprietario cadastrado:", this.state)
    try {
      await api.post('proprietarios', { nome, email, telefone, cpf, facebookID });
      this.props.navigation.navigate("ProprietarioInfo")
    } catch (error) {
      console.log(error)
    }
  }

  handleNomeChange = nome => {
    this.setState({
      ...this.state,
      nome
    });
  }

  handleEmailChange = email => {
    this.setState({
      ...this.state,
      email
    });
  }

  handleCpfChange = cpf => {
    this.setState({
      ...this.state,
      cpf
    });
  }

  handleTelefoneChange = telefone => {
    this.setState({
      ...this.state,
      telefone
    });
  }

  handleSenhaChange = senha => {
    this.setState({
      ...this.state,
      senha
    });
  }

  handleCancelarCadastro = () => {
    facebookService.makeLogoutButton((accessToken) => {
      this.logout()
    })
    this.props.navigation.navigate("Home")
  }

  render() {
    return (
      <ScrollView>
        <Header />
        <View style={styles.bodyBox}>
          <Text style={styles.textWelcome}>Complete o seu cadastro:</Text>

          <View style={styles.formCadastro}>
            <Text style={styles.textTipoLocal}>Seu cadastro</Text>
            <Input style={styles.inputNome} placeholder="Seu email" onChangeText={this.handleEmailChange} value={this.state.email} />
            <Input style={styles.inputNome} placeholder="Seu cpf" onChangeText={this.handleCpfChange} value={this.state.cpf} />
            <Input style={styles.inputNome} placeholder="Telefone" onChangeText={this.handleTelefoneChange} value={this.state.telefone} />
            <TouchableOpacity onPress={this.handleCadastroProprietario}><Text style={styles.buttonCadastro}>Cadastrar-se</Text></TouchableOpacity>
            <TouchableOpacity onPress={this.handleCancelarCadastro}><Text style={styles.buttonRealizarEvento}>Cancelar</Text></TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  bodyBox: {
    marginTop: 20
  },
  buttonGroup: {
    flexDirection: 'row',
    margin: 15
  },
  buttonTipoImovel: {
    marginLeft: 10,
    marginTop: 10
  },
  buttonCadastro: {
    backgroundColor: '#36B67E',
    width: 205,
    height: 50,
    borderRadius: 2,
    textAlign: 'center',
    paddingTop: 15,
    marginTop: 25,
    left: 35,
    color: '#FFF'
  },
  buttonRealizarEvento: {
    backgroundColor: '#FFF',
    width: 205,
    height: 50,
    borderRadius: 2,
    textAlign: 'center',
    paddingTop: 15,
    marginTop: 15,
    left: 35,
    color: '#36B67E',
    borderWidth: 0.75,
    borderColor: '#36B77E',
    marginBottom: 15
  },
  bannerAnuncio: {
    backgroundColor: '#5063f0',
    height: 100
  },
  formCadastro: {
    margin: 36,
    width: 300,
    borderColor: '#E5E5E5',
    borderWidth: 0.75,
    borderRadius: 2,
  },
  buttonText: {
    color: '#FFF',
    width: 205,
    height: 50,
    top: 30,
    left: 77,
    borderColor: '#FFF',
    borderWidth: 0.75,
    borderRadius: 2,
    textAlign: 'center',
    paddingTop: 15
  },
  textWelcome: {
    textAlign: 'center',
    fontSize: 18,
    fontWeight: 'bold',
    marginTop: 5
  },
  textWelcome2: {
    textAlign: 'center',
    fontSize: 14,
    marginTop: 5
  },
  textTipoLocal: {
    textAlign: 'center',
    fontSize: 14,
    fontWeight: 'bold',
    marginTop: 10
  },
  textInput: {
    width: 260,
    height: 50,
    margin: 60,
    borderWidth: 1,
    borderRadius: 2,
    borderColor: '#ddd',
    borderBottomWidth: 0,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1,
    padding: 10,
  },
  landscape: {
    marginLeft: 5
  }
});