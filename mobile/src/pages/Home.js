import React, { Component } from 'react';

import {
  ScrollView,
  Image,
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  BackHandler
} from 'react-native';

import Header from '../components/Header'
import Search from '../components/Search'

import FBSDK from 'react-native-fbsdk'

const { AccessToken } = FBSDK

export default class Home extends Component {
  state = {
    textBusca: '',
    accessToken: null
  };

  handleInputChange = (data, details) => {
    this.props.navigation.navigate("Mapa", { 
      coordenadas: details.geometry.location 
    })
  };

  componentDidMount() {
    AccessToken.getCurrentAccessToken()
      .then((response) => {
        this.setState({
          accessToken: response.accessToken
        })
      })
      .catch(error => {
      })
      BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
  };

  handleBackPress = () => {
    BackHandler.exitApp();
  }

  handleEntrarComoProprietario = () => {
    if (this.state.accessToken == null) {
      this.props.navigation.navigate("ProprietarioLogin")
    } else {
      this.props.navigation.navigate("ProprietarioSwitch")
    }
  }
  
  handleComeceSeuEvento = () => {
    this.props.navigation.navigate("CadastroCliente")
  }

  handleVerLocaisCadastrados = () => {
    this.props.navigation.navigate("ListaLocais")
  }

  handleLocationSelected = (data, { geometry } ) => {
    const { location: { lat: latitude, lng: longitude } } = geometry;
  }

  handleVerProprietarioCadastro = () => {
    this.props.navigation.navigate('ProprietarioCadastro')
  }

  handleCadastrarCliente = () => {
    this.props.navigation.navigate('CadastroCliente')
  }

  render() {
    return (
      <ScrollView behavior="padding" style={styles.container}>
        <Header />
        <View style={styles.bodyBox}>
          <Text style={styles.textWelcome}>O jeito fácil de fazer sua festa!!</Text>
          <Text style={styles.textWelcome2}>Sua festa rápida, online, segura e sem dor de cabeça</Text>
          <Search onPress={this.handleInputChange}/>
          <Image style={styles.landscape} source={require('./../../assets/images/landscape.png')}></Image>
          <View style={styles.bannerAnuncio}>
            <TouchableOpacity onPress={this.handleEntrarComoProprietario}><Text style={styles.buttonText}>Entrar como proprietário</Text></TouchableOpacity>
          </View>
          <View style={styles.bannerAnuncio}>
            <TouchableOpacity onPress={this.handleVerLocaisCadastrados}><Text style={styles.buttonText}>Ver locais</Text></TouchableOpacity>
          </View>
          <View style={styles.bannerAnuncio}>
            <TouchableOpacity onPress={this.handleCadastrarCliente}><Text style={styles.buttonText}>Cadastrar Cliente</Text></TouchableOpacity>
          </View>
        </View>        
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  bodyBox: {
    marginTop: 20
  },
  bannerAnuncio: {
    backgroundColor: '#5063f0',
    height: 90
  },
  buttonText: {
    color: '#FFF',
    width: 205,
    height: 50,
    top: 30,
    left: 77,
    borderColor: '#FFF',
    borderWidth: 0.75,
    borderRadius: 2,
    textAlign: 'center',
    paddingTop: 15
  },
  textWelcome: {
    textAlign: 'center',
    fontSize: 18,
    fontWeight: 'bold',
    marginTop: 5
  },
  textWelcome2: {
    textAlign: 'center',
    fontSize: 14,
    marginTop: 5
  },
  textInput: {
    width: 260,
    height: 50,
    margin: 60,
    borderWidth: 1,
    borderRadius: 2,
    borderColor: '#ddd',
    borderBottomWidth: 0,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1,
    padding: 10,
  },
  landscape: {
    marginLeft: 5,
    marginTop: 100
  }
});