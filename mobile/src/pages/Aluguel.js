import React, { Component } from 'react';
import api from '../services/api';
import DatePicker from '../components/react-native-datepicker';
import moment from 'moment';
import { View, StyleSheet, Text, Button, Alert } from 'react-native';
import Header from '../components/Header';

export default class Aluguel extends Component {
  constructor(props) {
    super(props);

    this.state = {
      local: {
        endereco: {
          titulo: null,
          logradouro: null,
          numero: null,
          bairro: null,
          cidade: null,
          cep: null,
          uf: null,
          pais: null,
          latitude: null,
          longitude: null,
        },
        _id: null,
        photos: [],
        precoDia: null,
        proprietario: null,
        alugueis: []
      },
      datetimeIn: '',
      datetimeOut: '',
      precoTotal: 0,
      alugueisLocal: {},
      disponivel: 2,
      dataValida: false,
      confirmado: false
    }
  }

  calculaPrecoAluguel = async () => {
    const days = (1 + moment(this.state.datetimeOut, "YYYY-MM-DD HH:mm").diff(moment(this.state.datetimeIn, "YYYY-MM-DD HH:mm"), 'days', true)).toFixed(0);
    const preco = (this.state.local.precoDia * days);
    if (preco > 0) {
      await this.setState({
        dataValida: true
      });
      this.setState({
        precoTotal: preco
      });
      this.isDisponivel();
    }
    else {
      await this.setState({
        dataValida: false
      });
      Alert.alert('Data inválida!!!');
    }
  }

  formatDateTime = (datetime) => {
    if (datetime == '')
      return ''
    else
      return moment(datetime).format("YYYY-MM-DD HH:mm");
  }

  async componentDidMount() {
    const id = this.props.navigation.getParam('idLocal');
    const response = await api.get("detalharLocal/" + id);
    this.setState({
      local: response.data
    })
    var url = '/local/' + id + '/alugueis';
    const res = await api.get(url);
    this.setState({
      alugueisLocal: res.data
    })
  }

  isDisponivel = async () => {
    await this.setState({
      disponivel: 1,
    })
    for (var i = 0; i < this.state.alugueisLocal.length; i++) {
      var aluguel = this.state.alugueisLocal[i];
      await this.calculoAgenda(this.state.datetimeIn, this.state.datetimeOut, aluguel.dataEntrada, aluguel.dataSaida);
      await this.calculoAgenda(aluguel.dataEntrada, aluguel.dataSaida, this.state.datetimeIn, this.state.datetimeOut);
    }
  }

  calculoAgenda = async (entrada1, saida1, entrada2, saida2) => {
    var beetweenIn = moment(entrada1).isBetween(moment(entrada2), moment(saida2));
    var beetweenOut = moment(saida1).isBetween(moment(entrada2), moment(saida2));
    if (beetweenIn || beetweenOut) {
      await this.setState({
        disponivel: 0,
      })
    }
  }

  resultado = () => {
    console.log('resultado:');
    alert(this.state.disponivel);

  }

  alugar = async () => {
    if (this.state.dataValida === true) {
      if (this.state.disponivel === 1) {
        // console.log(this.state.local._id);
        var url = "/local/" + this.state.local._id + "/aluguel";
        const aluguel = {
          "proprietario": this.state.local.proprietario,
          "local": this.state.local._id,
          "dataEntrada": this.state.datetimeIn,
          "dataSaida": this.state.datetimeOut,
          "precoTotal": await this.state.precoTotal.toString()
        }
        // console.log(response);
        // console.log(aluguel);
        const response = await api.post(url, aluguel);

        if (response.status == 200) {
          await this.setState({
            confirmado: true
          });
          Alert.alert('Aluguel reservado!');
          console.log(this.state.confirmado);
          // setTimeout( () => {
          //   this.props.navigation.navigate("Home")}, 3000);
        }
      }
      else {
        Alert.alert('Data inválida!!!');
      }
    }
    else {
      Alert.alert('Local indisponível nesta data, por favor, escolha outra.');
    }
  }


  componentDidUpdate() {
    console.log(this.state.dataValida);
  }
  render() {

    let textoDisponivel;
    if (this.state.disponivel === 0) {
      textoDisponivel = <Text style={{ fontSize: 25, color: "red", textAlign: 'center' }}> Indisponível</Text>;
    }

    return (

      <>
        <View style={styles.container}>
          <Header />
          <Text>{this.props.navigation.getParam('local')}</Text>
          <View style={{ padding: 30 }}>
            <Text>Selecione a data de entrada: </Text>
            <View style={styles.datetimeContainer}>
              <View style={{ flexDirection: 'row' }}>
                <DatePicker
                  style={{ width: 50 }}
                  date={this.formatDateTime(this.state.datetimeIn)}
                  mode="datetime"
                  format="YYYY-MM-DD HH:mm"
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  customStyles={{
                    dateIcon: {
                      position: 'absolute',
                      left: 0,
                      top: 4,
                      marginLeft: 0
                    },
                    dateInput: {
                      marginLeft: 36
                    }
                  }}
                  onDateChange={(datetime) => { this.setState({ datetimeIn: datetime }) }}
                />
                <Text>{this.state.datetimeIn}</Text>
              </View>
            </View>

            <Text>Selecione a data de saída: </Text>
            <View style={styles.datetimeContainer}>


              <View style={{ flexDirection: 'row' }}>
                <DatePicker

                  style={{ width: 50 }}
                  date={this.formatDateTime(this.state.datetimeOut)}
                  mode="datetime"
                  format="YYYY-MM-DD HH:mm"
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  customStyles={{
                    dateIcon: {
                      position: 'absolute',
                      left: 0,
                      top: 4,
                      marginLeft: 0
                    },
                    dateInput: {
                      marginLeft: 36
                    }
                  }}
                  onDateChange={(datetime) => { this.setState({ datetimeOut: datetime }) }}
                />
                <Text>{this.state.datetimeOut}</Text>
              </View>

            </View>
          </View>
          <Button
            title="Calcular Preço Total"
            onPress={this.calculaPrecoAluguel}
          />
          <Text>Preço Total: R${this.state.precoTotal},00</Text>

          <Text>{textoDisponivel}</Text>
          {
            (this.state.disponivel === 1) && (this.state.dataValida === true) && (!this.state.confirmado) && <Button
              title="Clique para Alugar"
              onPress={this.alugar}
            />
          }

          {
            (this.state.confirmado == true) 
            && <Text>Entre em contato com {this.state.local.proprietario.nome} para realizar o pagamento. Telefone: {this.state.local.proprietario.telefone}</Text> 
          }
          {
            (this.state.confirmado == true) && <Text>Email:{this.state.local.proprietario.email}</Text>
          }
        </View>
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  datetimeContainer: {
    flexDirection: 'row'
  }
});
