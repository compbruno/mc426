import React, { Component } from 'react';
import api from '../services/api';

import {
  View,
  Text,
  StyleSheet,
  Image,
  Button
} from 'react-native';

import Header from '../components/Header';
export default class ListaLocais extends Component {
  state = {
    endereco: {
      titulo: null,
      logradouro: null,
      numero: null,
      bairro: null,
      cidade: null,
      cep: null,
      uf: null,
      pais: null,
      latitude: null,
      longitude: null,
    },
    _id: null,
    photos: [],
    precoDia: null
  };

  async componentDidMount() {
    const local = this.props.navigation.getParam('idLocal');
    console.log(local);
    const response = await api.get("detalharLocal/" + local.id);
    console.log(response.data);
    this.setState(response.data);
    console.log(this.state.photos[0]);
  }

  verificarDisponibilidade = () => {
    console.log(this.state);
    this.props.navigation.navigate("Aluguel", { idLocal: this.state._id });
  }



  render() {
    const sourceUri = { uri: this.state.photos[0] };
    return (
      <View style={styles.container}>
        <Header />
        <View>
          <Text style={styles.titulo}>Local selecionado:</Text>
        </View>
        <Image style={{ width: 150, height: 150 }} source={sourceUri}></Image>
        <Text></Text>
        <Text>Título: {this.state.titulo}</Text>
        <Text>Preço/dia: R${this.state.precoDia}</Text>
        <Text />
        <Text>Endereço:</Text>
        <Text>{this.state.endereco.logradouro}, {this.state.endereco.numero}</Text>
        <Text>{this.state.endereco.bairro}, {this.state.endereco.cidade}</Text>
        <Text></Text>
        <Text></Text>
        <Text></Text>
        <Text></Text>
        <Button onPress={this.verificarDisponibilidade} title='Verificar disponibilidade' color="#36B67E"></Button>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
  }
});