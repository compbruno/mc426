import React, { Component } from "react";
import { View, BackHandler } from "react-native";
import MapView, { Marker } from "react-native-maps";
import api from '../services/api';

import Search from "../components/Search";

export default class Map extends Component {
  state = {
    region: null,
    locais: []
  };

  handleBackPress = () => {
    this.props.navigation.navigate("Home");
    return true;
  }

  async componentDidMount() {
    const response = await api.get("locais");
    this.setState({ locais: response.data });

    const coordenadas = this.props.navigation.getParam('coordenadas');

    this.setState({
      region: {
        latitude: coordenadas.lat,
        longitude: coordenadas.lng,
        latitudeDelta: 0.0143,
        longitudeDelta: 0.0134
      }
    });
    BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
  }

  handleSelecionarLocal = (localId) => {
    this.props.navigation.navigate("DetalheLocal", {
      idLocal: localId
    })
  }

  render() {
    const { region } = this.state;

    return(
      <View style={{ flex: 1 }}>
        <MapView
          style={{ flex: 1 }}
          region={ region }
          showsUserLocation
          loadingEnabled
        >
        {this.state.locais.map((local) => {
          let coords = {
            latitude: parseFloat(local.endereco.latitude),
            longitude: parseFloat(local.endereco.longitude)
          }
          return(
            <Marker
              coordinate={coords}
              key={local._id}
              identifier={local._id}
              title={local.titulo}
              onPress={(event) => this.handleSelecionarLocal(event.nativeEvent)}
            />
          )
        })}
        </MapView>
        <Search />
      </View>
    );
  }
}
