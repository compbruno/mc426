import React, { Component } from "react";

import {
  View,
  Text,
  StyleSheet
} from 'react-native';

import Header from '../components/Header';
import api from "../services/api";

export default class CadastroProprietario extends Component {
  state = {
    usuarios: [],
  };

  async componentDidMount() {
    const response = await api.get("usuarios");

    this.setState({ usuarios: response.data });
  }

  render() {
    return (
      <View>
        <Header />
        <Text>Lista!!!</Text>
        {this.state.usuarios.map(usuario => (
          <Text>{usuario.nome}</Text>
        ))}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  bodyBox: {
    marginTop: 20
  },
  buttonGroup: {
    flexDirection: 'row',
    margin: 15
  },
  buttonTipoImovel: {
    margin: 30
  },
  bannerAnuncio: {
    backgroundColor: '#5063f0',
    height: 100
  },
  formCadastro: {
    margin: 50,
    width: 300,
    borderColor: '#E5E5E5',
    borderWidth: 0.75,
    borderRadius: 2, 
  },
  buttonText: {
    color: '#000',
    width: 205,
    height: 50,
    top: 30,
    left: 77,
    borderColor: '#000',
    borderWidth: 0.75,
    borderRadius: 2, 
    textAlign: 'center',
    paddingTop: 15
  },
  textWelcome: {
    textAlign: 'center',
    fontSize: 18,
    fontWeight: 'bold',
    marginTop: 5
  },
  textWelcome2: {
    textAlign: 'center',
    fontSize: 14,
    marginTop: 5
  },
  textInput: {
    width: 260,
    height: 50,
    margin: 60,
    borderWidth: 1,
    borderRadius: 2,
    borderColor: '#ddd',
    borderBottomWidth: 0,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1,
    padding: 10,
  },
  landscape: {
    marginLeft: 5
  }
});