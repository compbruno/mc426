import React, { Component } from 'react';
import api from '../services/api';
import {
  ScrollView,
  StyleSheet
} from 'react-native';
import FBSDK from 'react-native-fbsdk'

const { AccessToken } = FBSDK

export default class ProprietarioSwitch extends Component {
  state = {
    facebookID: "",
    proprietario: null
  }


  async componentDidMount() {
    AccessToken.getCurrentAccessToken()
      .then((response) => {
        fetch('https://graph.facebook.com/v2.5/me?access_token=' + response.accessToken)
          .then((response2) => response2.json())
          .then((json) => {
            this.setState({ facebookID: json.id })
          })
          .then(async () => {
            try {
              const response = await api.get("proprietario/facebookID/" + this.state.facebookID);
              this.setState({ proprietario: response.data });
            } catch (error) {
              console.log(error)
            }
          })
          .then(() => {
            if (this.state.proprietario == null) {
              this.props.navigation.navigate('ProprietarioCadastro');
            } else {
              this.props.navigation.navigate('ProprietarioInfo');
            }
          })
          .done();
      })
      .catch(error => {
        console.log(error)
      })
  }

  render() {
    return (
      <ScrollView behavior="padding" style={styles.container}>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  }
})