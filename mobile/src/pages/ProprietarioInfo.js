import React, { Component } from 'react';
import api from '../services/api';
import { facebookService } from '../services/FacebookService'
import {
  ScrollView,
  View,
  Text,
  TouchableOpacity,
  StyleSheet
} from 'react-native';

import Header from '../components/Header'

import FBSDK from 'react-native-fbsdk'
const { AccessToken } = FBSDK

export default class ProprietarioInfo extends Component {
  state = {
    nome: "",
    email: "",
    telefone: "",
    cpf: "",
    facebookID: "",
  };

  async componentDidMount() {
    AccessToken.getCurrentAccessToken()
      .then((response) => {
        fetch('https://graph.facebook.com/v2.5/me?access_token=' + response.accessToken)
          .then((response2) => response2.json())
          .then((json) => {
            this.setState({
              facebookID: json.id,
              nome: json.name
            })
          })
          .then(async () => {
            try {
              const response = await api.get("proprietario/facebookID/" + this.state.facebookID);
              this.setState({
                email: response.data.email,
                telefone: response.data.telefone,
                cpf: response.data.cpf
              });
            } catch (error) {
              console.log(error)
            }
          })
          .done();
      })
      .catch(error => {
        console.log(error)
      })
  }

  handleProprietarioListaLocais = () => {
    this.props.navigation.navigate("ListaLocaisPorProprietario")
  }

  handleProprietarioAddLocal = () => {
    this.props.navigation.navigate("CadastroLocal")
  }

  handleVoltar = () => {
    this.props.navigation.navigate("Home")
  }

  render() {
    return (
      <ScrollView behavior="padding" style={styles.container}>
        <Header />
        <View style={styles.bodyBox}>
          <Text style={styles.textWelcome}>Página do proprietário</Text>
          <Text style={styles.textWelcome2}>Nome: {this.state.nome}</Text>
          <Text style={styles.textWelcome2}>Email: {this.state.email}</Text>
          <Text style={styles.textWelcome2}>Telefone: {this.state.telefone}</Text>
          <Text style={styles.textWelcome2}>CPF: {this.state.cpf}</Text>
        </View>
        <View style={styles.formCadastro}>
          <TouchableOpacity onPress={this.handleProprietarioListaLocais}><Text style={styles.buttonCadastroProp}>Lista de locais cadastrados</Text></TouchableOpacity>
          <TouchableOpacity onPress={this.handleProprietarioAddLocal}><Text style={styles.buttonCadastroProp}>Cadastrar novo local</Text></TouchableOpacity>
          <TouchableOpacity onPress={this.handleVoltar}><Text style={styles.buttonCadastroProp}>Voltar</Text></TouchableOpacity>
          <View style={styles.buttonCadastroProp}>
            {facebookService.makeLogoutButton((accessToken) => {
              this.logout()
            })}
          </View>
        </View>
      </ScrollView>
    );
  }

  logout() {
    this.props.navigation.navigate('Home')
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  bodyBox: {
    marginTop: 20
  },
  buttonCadastroProp: {
    backgroundColor: '#FFF',
    width: 205,
    height: 50,
    borderRadius: 2,
    textAlign: 'center',
    paddingTop: 15,
    marginTop: 15,
    left: 35,
    color: '#36B67E',
    borderWidth: 0.75,
    borderColor: '#36B77E',
    marginBottom: 15
  },
  bannerAnuncio: {
    backgroundColor: '#5063f0',
    height: 100
  },
  buttonText: {
    color: '#FFF',
    width: 205,
    height: 50,
    top: 30,
    left: 77,
    borderColor: '#FFF',
    borderWidth: 0.75,
    borderRadius: 2,
    textAlign: 'center',
    paddingTop: 15
  },
  textWelcome: {
    textAlign: 'center',
    fontSize: 18,
    fontWeight: 'bold',
    marginTop: 5
  },
  textWelcome2: {
    textAlign: 'center',
    fontSize: 14,
    marginTop: 5
  },
  textInput: {
    width: 260,
    height: 50,
    margin: 60,
    borderWidth: 1,
    borderRadius: 2,
    borderColor: '#ddd',
    borderBottomWidth: 0,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1,
    padding: 10,
  },
  landscape: {
    marginLeft: 5,
    marginTop: 170
  },
  formCadastro: {
    margin: 36,
    width: 300,
    borderColor: '#E5E5E5',
    borderWidth: 0.75,
    borderRadius: 2,
  },
});