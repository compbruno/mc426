import { createSwitchNavigator } from 'react-navigation'
import ProprietarioCadastro from '../../pages/ProprietarioCadastro'
import ProprietarioInfo from '../../pages/ProprietarioInfo'
import ListaLocaisPorProprietario from '../../pages/ListaLocaisPorProprietario'
import ProprietarioSwitch from '../../pages/ProprietarioSwitch'
import Home from '../../pages/Home'
import CadastroLocal from '../../pages/CadastroLocal'

const MainNavigator = createSwitchNavigator({
  Home: {
    screen: Home,
    navigationOptions: {
      title: "Home"
    }
  },
  ProprietarioCadastro: {
    screen: ProprietarioCadastro
  },
  ProprietarioInfo: {
    screen: ProprietarioInfo
  },
  ListaLocaisPorProprietario: {
    screen: ListaLocaisPorProprietario
  },
  ProprietarioSwitch: {
    screen: ProprietarioSwitch
  },
  CadastroLocal: {
    screen: CadastroLocal
  }
})

export default MainNavigator;