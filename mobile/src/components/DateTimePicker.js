import React, { Component } from 'react'
import { View } from 'react-native';
import DatePicker from '../components/react-native-datepicker';

export default class DateTimePicker extends Component {
    constructor(props) {
        super(props);
        this.state = {
            datetime: ''
        };

    }

    getTheState() {
        return this.state;
    }

    render() {
        return (
            <>
                <View style={{ flexDirection: 'row' }}>
                    <DatePicker

                        style={{ width: 200 }}
                        date={this.state.datetime}
                        mode="datetime"
                        format="YYYY-MM-DD HH:mm"
                        confirmBtnText="Confirm"
                        cancelBtnText="Cancel"
                        customStyles={{
                            dateIcon: {
                                position: 'absolute',
                                left: 0,
                                top: 4,
                                marginLeft: 0
                            },
                            dateInput: {
                                marginLeft: 36
                            }
                        }}
                        onDateChange={(datetime) => { this.setState({ datetime: datetime }) }}
                    />
                </View>
            </>
        );
    }
}