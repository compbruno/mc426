import React, { Component } from 'react';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';

export default class Search extends Component {
  constructor(props){
    super(props);
  }

  handleVerRegiao = (data, details) => {
    this.props.onPress(data, details);
  }

  render() {
    
    return (
      <GooglePlacesAutocomplete
        placeholder="Qual região você gostaria?"
        onPress={this.handleVerRegiao}
        query={
          {
            key: 'AIzaSyCHq7PkbOxaBsgOXArvXhA9hROenst8z9o',
            language: 'pt',
          }
        }
        textInputProps={
          {
            autoCapitalize: "none",
            autoCorrect: false
          }
        }
        fetchDetails
        enablePoweredByContainer={false}
        styles={
          {
            container: {
              position: 'absolute',
              top: 80,
              width: "100%",
              zIndex: 1,
              marginBottom: 40
            },
            textInputContainer: {
              flex:1,
              backgroundColor: 'transparent',
              height: 54,
              marginHorizontal: 30,
              borderTopWidth: 0,
              borderBottomWidth: 0
            },
            textInput: {
              height: 50,
              margin: 0,
              borderRadius: 0,
              paddingTop: 0,
              paddingLeft: 20,
              paddingRight: 20,
              paddingBottom: 0,
              marginTop: 0,
              marginLeft: 0,
              marginRight: 0,
              elevation: 5,
              shadowColor: "#000",
              shadowOpacity: 0.1,
              shadowOffset: { x: 0, y: 0 },
              shadowRadius: 15,
              borderWidth: 1,
              borderColor: "#DDD",
              fontSize: 18
            },
            listView: {
              borderWidth: 1,
              borderColor: "#DDD",
              backgroundColor: "#FFF",
              marginHorizontal: 20,
              elevation: 5,
              marginTop: 10,
              shadowColor: "#000",
              shadowOpacity: 0.1,
              shadowOffset: { x: 0, y: 0 },
              shadowRadius: 15,
            },
            description: {},
            row: {},
          }
        }
      />
    )
  }
}