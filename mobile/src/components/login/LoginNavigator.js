import { createSwitchNavigator } from 'react-navigation'
import ProprietarioLogin from '../../pages/ProprietarioLogin'
import Home from '../../pages/Home'
import ListaLocais from '../../pages/ListaLocais'
import Aluguel from '../../pages/Aluguel'
import Mapa from '../../pages/Mapa'
import DetalheLocal from '../../pages/DetalheLocal'
import CadastroCliente from '../../pages/CadastroCliente'


const LoginNavigator = createSwitchNavigator({
  Home: {
    screen: Home,
    navigationOptions: {
      title: "Home"
    }
  },
  ProprietarioLogin: {
    screen: ProprietarioLogin,
    navigationOptions: {
      title: "ProprietarioLogin"
    }
  },
  ListaLocais: {
    screen: ListaLocais,
    navigationOptions: {
      title: "ListaLocais"
    }
  },
  Aluguel: {
    screen: Aluguel,
    navigationOptions: {
      title: "Aluguel"
    }
  },
  Mapa: {
    screen: Mapa,
    navigationOptions: {
      title: "Mapa"
    }
  },
  DetalheLocal: {
    screen: DetalheLocal,
    navigationOptions: {
      title: "DetalheLocal"
    }
  },
  CadastroCliente: {
    screen: CadastroCliente,
    navigationOptions: {
      title: "CadastroCliente"
    }
  }
})

export default LoginNavigator;