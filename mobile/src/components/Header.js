import React, { Component } from "react";

import { View, StyleSheet, Image, Text, TouchableHighlight } from "react-native";

export default class Header extends Component {
  _handleGoHome = () => {
    this.props.navigation.navigate("Home");
  }

  render() {
    return (
      <View style={styles.headerBox}>
        <TouchableHighlight onPress={this._handleGoHome}>
          <Image style={styles.logo} source={require('./../../assets/images/logo.png')}></Image>
        </TouchableHighlight>
        <Text style={styles.textMarca}>Rent4<Text style={styles.textMarcaBold}>Event</Text></Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  logo: {
    width: 48,
    height: 48,
    marginLeft: 20,
    marginTop: 25
  },
  headerBox: {
    height: 78,
    backgroundColor: '#FFFFFF',
    textAlign:'center',
    flexDirection: 'row'
  },
  textMarca: {
    fontFamily: 'roboto',
    fontSize: 36,
    textAlign: 'center',
    color: '#36B67E',
    marginLeft: 35,
    marginTop: 25
  },
  textMarcaBold: {
    fontWeight: 'bold'
  },
});