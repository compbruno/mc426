import React, { Component } from 'react';
import LoginNavigator from './components/login/LoginNavigator'
import MainNavigator from './components/main/MainNavigator'
import FBSDK from 'react-native-fbsdk'
import { createSwitchNavigator, createAppContainer } from 'react-navigation';

const { AccessToken } = FBSDK

export default class Routes extends Component {

  constructor(props) {
    super(props)

    this.state = {
      accessToken: null
    }
  }

  componentDidMount() {
    AccessToken.getCurrentAccessToken()
      .then((data) => {
        if (data)
          this.setState({
            accessToken: data.accessToken
          })
      })
      .catch(error => {
        console.log(error)
      })
  }

  render() {
    const Navigator = createAppContainer(makeRootNavigator(this.state.accessToken))
    return <Navigator />
  }
}

const makeRootNavigator = (isLoggedIn) => {
  return createSwitchNavigator(
    {
      LoginNavigator: {
        screen: LoginNavigator
      },
      MainNavigator: {
        screen: MainNavigator
      }
    },
    {
      initialRouteName: isLoggedIn ? "MainNavigator" : "LoginNavigator"
    }
  )
}