import axios from 'axios';

const api = axios.create({
  baseURL: 'https://rent4event.herokuapp.com/',
});

export default api;