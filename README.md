# Processo de levantamento de requisitos
[Elicitação](https://docs.google.com/document/d/1C_lVWoROGbX9mTyK5xwKjipC4LhKyPiArRxNb05MGj4/edit?usp=sharing)

# Construção automatizada da aplicação e Container Docker
Como nosso projeto é em javascript, utilizamos o Yarn como ferramenta de construção automatizada da aplicação 

# Arquitetura Rent4Event
![Diagrama](https://i.imgur.com/4btCu95.jpg)