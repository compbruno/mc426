const Usuario = require('../models/Usuario');

module.exports = {
  async index(req, res) {
    const usuarios = await Usuario.find({}).sort('-dataCadastro');
    return res.json(usuarios);
  },
  async store(req, res) {
    const usuario = await Usuario.create(req.body);
    return res.json(usuario)
  },
  async delete(req, res) {
    const usuario = await Usuario.deleteOne(req.body);
    return res.send("Got a DELETE request at /usuario")
  }
};