const Local = require('../models/Local');
const Proprietario = require('../models/Proprietario');

module.exports = {
  async index(req, res) {
    const locais = await Local.find();
    return res.json(locais);
  },
  async store(req, res) {
    const proprietario = await Proprietario.findById(req.params.id);
    const local = await Local.create(req.body);
    proprietario.locais.push(local);
    proprietario.save();
    return res.json(local);
  },
  async findById(req, res){
    const local = await Local.findById(req.params.idLocal).populate('proprietario');
    return res.json(local);
  },
  async delete(req, res) {
    const proprietario = await Proprietario.findById(req.params.id);
    const local = await Local.findByIdAndDelete(req.params.idLocal);
    proprietario.locais.pull(local);
    proprietario.save();
    return res.json(proprietario);
  },
  async update(req, res) {
    const local = await Local.findByIdAndUpdate(
        req.params.id,
        req.body,
        {new: true, overwrite: false},
        (err, dados) => {
            if (err) return res.status(500).send(err);
            return res.send(dados);
        }
    ) 
  }
};