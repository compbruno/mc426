const Aluguel = require('../models/Aluguel');
const Local = require('../models/Local');

module.exports = {
  async index(req, res) {
    const alugueis = await Aluguel.find();
    return res.json(alugueis);
  },
  async getOne(req, res) {
    if (req.params.id == null) {
      return res.status(401).json({ message: "Incorrect id" })
    }
    const aluguel = await Aluguel.findById(req.params.id);
    
    return res.json(aluguel);
  },
  async store(req, res) {
    const local = await Local.findById(req.params.id);
    const aluguel = await Aluguel.create(req.body);
    local.alugueis.push(aluguel);
    local.save();
    return res.json(aluguel);
  },
  async update(req, res) {
    const aluguel = await Aluguel.findByIdAndUpdate(
      req.params.id,
      req.body,
      { new: true, overwrite: false },
      (err, dados) => {
        if (err) return res.status(500).send(err);
        return res.send(dados);
      }
    )
  },
  async getAlugueisByIdLocal(req, res) {
    const idLocal = req.params.idLocal;
    const alugueis = await Aluguel.find().where({
      'local': idLocal
    });
    return res.json(alugueis);
  }
};