const Proprietario = require('../models/Proprietario');

module.exports = {
  async index(req, res) {
    const proprietarios = await Proprietario.find({}).sort('-dataCadastro').populate('locais');
    return res.json(proprietarios);
  },
  async store(req, res) {
    const proprietario = await Proprietario.create(req.body);
    return res.json(proprietario)
  },
  async delete(req, res) {
    const proprietario = await Proprietario.deleteOne(req.body);
    return res.send("Got a DELETE request at /proprietario")
  },
  async getProprietario(req, res) {
    const proprietarios = await Proprietario.findById(req.params.id).populate('locais');
    return res.json(proprietarios);
  },
  async getProprietarioByEmail(req, res) {
    const proprietario = await Proprietario.findOne({ email: req.params.email }).populate('locais');
    return res.json(proprietario);
  },
  async getProprietarioByFbID(req, res) {
    const proprietario = await Proprietario.findOne({ facebookID: req.params.facebookID }).populate('locais');
    return res.json(proprietario);
  },
  async getLocais(req, res) {
    const proprietario = await Proprietario.findById(req.params.id).populate('locais');
    return res.json(proprietario.locais);
  },
  async update(req, res) {
    const proprietario = await Proprietario.findByIdAndUpdate(
      req.params.id,
      req.body,
      { new: true },
      (err, dados) => {
        if (err) return res.status(500).send(err);
        return res.send(dados);
      }
    )
    return proprietario;
  }
};