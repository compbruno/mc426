const express = require('express');

const routes = express.Router();

const UsuarioController = require('./controllers/UsuarioController');
const LocalController = require('./controllers/LocalController');
const ProprietarioController = require('./controllers/ProprietarioController');
const AluguelController = require('./controllers/AluguelController');

routes.get('/usuarios', UsuarioController.index);
routes.post('/usuarios', UsuarioController.store);
routes.delete('/usuario/:id', UsuarioController.delete);

routes.get('/locais', LocalController.index);
routes.get('/detalharLocal/:idLocal', LocalController.findById);
routes.post('/proprietario/:id/local', LocalController.store);
routes.delete('/proprietario/:id/local/:idLocal', LocalController.delete);
routes.put('/local/:id', LocalController.update);

routes.get('/proprietarios', ProprietarioController.index);
routes.get('/proprietario/:id', ProprietarioController.getProprietario);
routes.get('/proprietario/email/:email', ProprietarioController.getProprietarioByEmail);
routes.get('/proprietario/facebookID/:facebookID', ProprietarioController.getProprietarioByFbID);
routes.get('/proprietario/:id/locais', ProprietarioController.getLocais);
routes.post('/proprietarios', ProprietarioController.store);
routes.put('/proprietario/:id', ProprietarioController.update);
routes.delete('/proprietario/:id', ProprietarioController.delete);

routes.get('/alugueis', AluguelController.index);
routes.get('/aluguel/:id',AluguelController.getOne);
routes.post('/local/:idLocal/aluguel', AluguelController.store);
routes.put('/aluguel/:id', AluguelController.update);
routes.get('/local/:idLocal/alugueis',AluguelController.getAlugueisByIdLocal);

module.exports = routes;