const mongoose = require('mongoose');

 const LocalSchema = new mongoose.Schema({
   titulo: String,
   endereco: {
     logradouro: String,
     numero: String,
     bairro: String,
     cidade: String,
     cep: String,
     uf: String,
     pais: String,
     latitude: String,
     longitude: String,
   },
   precoDia: Number,
   proprietario: {
     type:mongoose.Schema.Types.ObjectId,
     ref: 'Proprietario',
     require: true,
   },
    photos: [
      String
    ],
    alugueis: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Aluguel',
      require: false
    }]
 })

 module.exports = mongoose.model('Local', LocalSchema);