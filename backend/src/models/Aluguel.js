const mongoose = require('mongoose');

const AluguelSchema = new mongoose.Schema({
    proprietario: {
        type:mongoose.Schema.Types.ObjectId,
        ref: 'Proprietario',
        require: true,
    },
    local: {
        type:mongoose.Schema.Types.ObjectId,
        ref: 'Local',
        require: true,
    },
    dataEntrada: {
        type: String,
        required: true
    },
    dataSaida: {
        type: String,
        required: true
    },
    precoTotal: Number
})

module.exports = mongoose.model('Aluguel', AluguelSchema);