const mongoose = require('mongoose');

const ClienteSchema = new mongoose.Schema({
  dadosPessoais: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Usuario',
    require: true
  }
});

module.exports = mongoose.model('Cliente', ClienteSchema);