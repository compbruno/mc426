const mongoose = require('mongoose');

const ProprietarioSchema = new mongoose.Schema({
    nome: String,
    email: String,
    telefone: String,
    cpf: String,
    facebookID: String,
    locais: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Local',
        require: false
    }],
    dataCadastro: {
      type: Date,
      default: Date.now,
    },
})

module.exports = mongoose.model('Proprietario', ProprietarioSchema);