const mongoose = require('mongoose');

const UsuarioSchema = new mongoose.Schema({
  nome: {
    type: String,
    require: true
  },
  email: {
    type: String,
    require: true
  },
  telefone: {
    type: String,
    require: true
  },
  cpf: {
    type: String,
    require: true
  },
  senha: {
    type: String,
    require: true
  },
  dataCadastro: {
    type: Date,
    default: Date.now,
    require: true,
  }
});

module.exports = mongoose.model('Usuario', UsuarioSchema);