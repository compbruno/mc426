import chai, { expect } from 'chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';

chai.use(sinonChai);

import { index, getOne, store, update, getAlugueisByIdLocal } from '../src/controllers/AluguelController';
import Aluguel from '../src/models/Aluguel';

describe ('Aluguel', () => {
  let mockedRes;

  before(() => {
    mockedRes = {
      send: () => { },
      json: (err) => {
          return {};
      },
      status: (responseStatus) => {
          assert.equal(responseStatus, 404);
          return this;
      }
    }
  });

  describe('smoke tests', () => {
      it('should exist the index method', () => {
        expect(index).to.exist;
      });

      it('should exist the getOne method', () => {
        expect(getOne).to.exist;
      });

      it('should exist the store method', () => {
        expect(store).to.exist;
      });

      it('should exist the update method', () => {
        expect(update).to.exist;
      });

      it('should exist the getAlugueisByIdLocal method', () => {
        expect(getAlugueisByIdLocal).to.exist;
      });
  });

  describe('Generic search', () => {
    it('should call find function', () => {
      const findStub = sinon.stub(Aluguel, "find");
      findStub.returns({})
      const alugueis = index({}, mockedRes);

      expect(findStub).to.have.been.calledOnce;
    });
    
  });

  describe('Search one "aluguel" by Id', () => {
    let findByIdStub;
    let mockedReqFindOneById;

    before(() => {
      findByIdStub = sinon.stub(Aluguel, "findById");
      mockedReqFindOneById = {
        params: {
          id: '5d164abbe9f55d0004a74242'
        }
      }
    });
    
    context('correct id format', () => {
      it('should return a 200 response status', () => {

        findByIdStub.returns({});
        const aluguel = getOne(mockedReqFindOneById, mockedRes);

        expect(findByIdStub).to.have.been.calledOnce;
      });
    });

  });
});